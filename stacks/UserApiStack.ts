import { StackContext, Api, Table, Config } from "sst/constructs";
import { BillingMode } from "aws-cdk-lib/aws-dynamodb";

export function UserApiStack({ stack }: StackContext) {
  // const table = new Table(stack, "Pixeme", {
  //   fields: {
  //     PK: "string",
  //     SK: "string"
  //   },
  //   primaryIndex: {
  //     partitionKey: "PK",
  //     sortKey: "SK"
  //   },
  //   cdk: {
  //     table: {
  //       billingMode: BillingMode.PROVISIONED,
  //       readCapacity: 12,
  //       writeCapacity: 12,
  //       pointInTimeRecovery: false
  //     }
  //   },
  // });

  // stack.setDefaultFunctionProps({
  //   environment: {
  //     DYNAMODB_TABLE_NAME: table.tableName
  //   }
  // })

  const api = new Api(stack, "api", {
    routes: {
      "GET /": "backend/functions/simple_lambda.handler",

      // Account APIs
      "POST /login": "backend/functions/simple_lambda.handler",
      "POST /logout": "backend/functions/simple_lambda.handler",
      "POST /account/create": "backend/functions/simple_lambda.handler",
      "PUT /account/change-password": "backend/functions/simple_lambda.handler",
      "POST /reset-password": "backend/functions/simple_lambda.handler",
      "GET /refresh": "backend/functions/simple_lambda.handler",
      "POST /account/disable": "backend/functions/simple_lambda.handler",
      "DELETE /account": "backend/functions/simple_lambda.handler",
      "PUT /account/profile": "backend/functions/simple_lambda.handler",
      "POST /account/profile/avatar": "backend/functions/simple_lambda.handler",
      "DELETE /account/profile/avatar": "backend/functions/simple_lambda.handler",
      "PUT /account/membership": "backend/functions/simple_lambda.handler",
      "GET /account/notifications": "backend/functions/simple_lambda.handler",
      "GET /account/notifications/{id}": "backend/functions/simple_lambda.handler",
      "PUT /account/notifications/{id}/read": "backend/functions/simple_lambda.handler",
      "DELETE /account/notifications/{id}": "backend/functions/simple_lambda.handler",
      "GET /account/lists": "backend/functions/simple_lambda.handler",
      "GET /account/images": "backend/functions/simple_lambda.handler",
      "GET /account/comments": "backend/functions/simple_lambda.handler",
      "DELETE /account/comments/{id}": "backend/functions/simple_lambda.handler",

      // User APIs
      "GET /users/{id}": "backend/functions/simple_lambda.handler",
      "GET /users": "backend/functions/simple_lambda.handler",
      "GET /users/{id}/images": "backend/functions/simple_lambda.handler",
      "GET /users/{id}/lists": "backend/functions/simple_lambda.handler",

      // Language APIs
      "GET /languages": "backend/functions/simple_lambda.handler",
      "GET /languages/{code}": "backend/functions/simple_lambda.handler",
      "GET /languages/{code}/lexemes": "backend/functions/simple_lambda.handler",
      "GET /languages/{code}/lexemes/newest": "backend/functions/simple_lambda.handler",
      "PUT /languages/{code}/fluency": "backend/functions/simple_lambda.handler",

      // List APIs
      "POST /lists": "backend/functions/simple_lambda.handler",
      "PUT /lists/{id}/name": "backend/functions/simple_lambda.handler",
      "POST /lists/{id}": "backend/functions/simple_lambda.handler",
      "DELETE /lists/{id}/items/{item}": "backend/functions/simple_lambda.handler",
      "GET /lists/{id}": "backend/functions/simple_lambda.handler",

      // Image APIs
      "GET /images": "backend/functions/simple_lambda.handler",
      "GET /images/{id}": "backend/functions/simple_lambda.handler",
      "GET /lexemes/{lang}/{lexeme}/images": "backend/functions/simple_lambda.handler",
      "POST /images": "backend/functions/simple_lambda.handler",
      "POST /images/{id}/lexemes": "backend/functions/simple_lambda.handler",
      "POST /images/{id}/sentences": "backend/functions/simple_lambda.handler",
      "DELETE /images/{id}/sentences/{id}": "backend/functions/simple_lambda.handler",
      "POST /images/{id}/tags": "backend/functions/simple_lambda.handler",
      "POST /images/{id}/comments": "backend/functions/simple_lambda.handler",
      "GET /images/{id}/comments": "backend/functions/simple_lambda.handler",

      // Sentence APIs
      "GET /sentences/{id}": "backend/functions/simple_lambda.handler",
      "POST /sentences/{id}/audios": "backend/functions/simple_lambda.handler",
      "POST /sentences/{id}/tags": "backend/functions/simple_lambda.handler",
      "DELETE /sentences/{id}/audios/{audio}": "backend/functions/simple_lambda.handler",
      "GET /sentences/{id}/comments": "backend/functions/simple_lambda.handler",
      "GET /sentences/{id}/audios/{audio}/comments": "backend/functions/simple_lambda.handler",
      "POST /sentences/{id}/comments": "backend/functions/simple_lambda.handler",
      "POST /sentences/{id}/audios/{audio}/comments": "backend/functions/simple_lambda.handler",
      "POST /sentences/{id}/question": "backend/functions/simple_lambda.handler",

    },
  });

  stack.addOutputs({
    ApiEndpoint: api.url,
    // TableName: table.tableName,
  });
}
