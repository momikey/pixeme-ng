import type { SSTConfig } from "sst";

import { UserApiStack } from "./stacks/UserApiStack"

export default {
  config(input) {
    return {
      name: "pixeme",
      region: "us-east-1",
    };
  },
  stacks(app) {
    app.setDefaultFunctionProps({
        runtime: "python3.9",
        memorySize: 512,
        timeout: 5,
        tracing: "disabled",
      });
    app.stack(UserApiStack);
  },
} satisfies SSTConfig;