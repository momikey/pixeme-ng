from datetime import datetime
import pytest

def test_lambda():
    from functions.simple_lambda import handler

    event = {
        "requestContext": { "time": datetime.now() }
    }

    context = None

    response = handler(event, context)

    assert response["statusCode"] == 200
