import pytest

from datetime import datetime

from db.models.tables import *


def test_create_image_item(dynamodb_table):
    image_item = ImageTable("IMG#123", "IMG#123", file="asdf", id="123", user="123")
    image_item.save()
    assert image_item.pk is not None
    assert image_item.sk is not None
    assert image_item.file == "asdf"
    assert not image_item.private


def test_create_lexeme_item(dynamodb_table):
    lexeme_item = LexemeTable("LEX#en", "LEX#en#test", lang="en", text="test", itpk="LEX", time=datetime.now())
    lexeme_item.save()
    assert lexeme_item.pk == "LEX#en"
    assert lexeme_item.sk == "LEX#en#test"
    assert lexeme_item.lang == "en"
    assert lexeme_item.text == "test"


def test_create_sentence_item(dynamodb_table):
    sentence_item = SentenceTable("SENT#en#123", "SENT#en#123", text="testing", user="1234", lang="en", id="foo", image="bar", status="A")
    sentence_item.save()
    assert sentence_item.pk is not None
    assert sentence_item.sk is not None


def test_create_audio_item(dynamodb_table):
    audio_item = AudioTable("SENT#en#123", "AUD#123", file="test", user="1234", lang="en", id="foo", sentence="bar")
    audio_item.save()
    assert audio_item.pk is not None
    assert audio_item.sk is not None


def test_create_tag_item(dynamodb_table):
    tag_item = TagTable("##TAG", "TAG#foo", text="testing")
    tag_item.save()
    assert tag_item.pk is not None
    assert tag_item.sk is not None


def test_create_user_item(dynamodb_table):
    user_item = UserTable("USER#123", "USER#123", id="foo", name="Tester", email="test@example.com", role=1)
    user_item.save()
    assert user_item.pk is not None
    assert user_item.sk is not None


def test_create_notification_item(dynamodb_table):
    notification_item = NotificationTable("USER#123", "MSG#123", text="testing", user="1234", id="foo", src="url", link="bar", read=False)
    notification_item.save()
    assert notification_item.pk is not None
    assert notification_item.sk is not None


def test_create_list_item(dynamodb_table):
    list_item = ListTable("USER#123", "LIST#123#456", user="1234", id="foo", name="test list", updated=datetime.now(), list_type="S", contents=["1","2","3"])
    list_item.save()
    assert list_item.pk is not None
    assert list_item.sk is not None
    assert len(list_item.contents) == 3
    assert "1" in list_item.contents


def test_create_language_item(dynamodb_table):
    language_item = LanguageTable("##LANG", "LANG#en", lang="en", name="test", native="test")
    language_item.save()
    assert language_item.pk is not None
    assert language_item.sk is not None


def test_create_comment_item(dynamodb_table):
    comment_item = CommentTable("USER#123", "COMM#123", text="testing", user="1234", id="foo", comment_type="S", parent_id="123", status="P")
    comment_item.save()
    assert comment_item.pk is not None
    assert comment_item.sk is not None


def test_create_fluency_item(dynamodb_table):
    fluency_item = FluencyTable("USER#123", "LANG#123", user="1234", lang="en", level=5)
    fluency_item.save()
    assert fluency_item.pk is not None
    assert fluency_item.sk is not None


def test_create_profile_item(dynamodb_table):
    profile_item = ProfileTable("USER#123", "USER#123#PROF", user="1234", updated=datetime.now(), profile={"foo": "bar"}, itpk="PROF", time=datetime.now())
    profile_item.save()
    assert profile_item.pk is not None
    assert profile_item.sk is not None
    assert "foo" in profile_item.profile.as_dict()


def test_create_lexeme_from_model(dynamodb_table):
    model = Lexeme("test", "en")
    item = LexemeTable.from_model(model)
    item.save()
    assert item.pk == "LEX#en"
    assert item.sk == "LEX#en#test"


def test_convert_lexeme_to_model(dynamodb_table):
    item = LexemeTable("LEX#en", "LEX#en#test", lang="en", text="test")
    item.save()
    returned = item.get("LEX#en", "LEX#en#test")
    model = returned.to_model()
    assert model.language == "en"
    assert model.lexeme == "test"
