import pytest

from db.models.item_list import ItemList, ListType

def test_create_item_list():
    item_list = ItemList(name="test", creator_id=123, contents=[1,2,3,4], type=ListType.lexemes)

    assert item_list is not None
    assert item_list.name is not None
    assert item_list.type is not None
    assert item_list.type.value == "L"
