from datetime import datetime
import pytest

from db.models.comment import Comment, CommentType

def test_create_comment():
    comment = Comment(text="test", time_created=datetime.now(), user_id=42, parent_reference=123, type=CommentType.image)

    assert comment is not None
    assert comment.id is not None
    assert comment.text is not None

