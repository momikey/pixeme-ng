import pytest

from db.dynamo.image import add_image
from db.dynamo.tag import add_tag_to_image, add_tag_to_sentence, get_images_with_tag, get_sentences_with_tag, remove_tag_from_image, remove_tag_from_sentence

from db.models.image import Image
from db.models.sentence import Sentence
from db.models.tables import CoreTableBase, ImageTable, SentenceTable
from db.models.tag import Tag

@pytest.fixture(scope="function")
def mock_image_and_sentence(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    sentence = Sentence(image.id, "this is a test", "en", "foo")
    sentence_item = SentenceTable.from_model(sentence)
    sentence_item.save()

    return (image, sentence)


def test_create_tag():
    tag = Tag("test")

    assert tag is not None
    assert tag.text is not None

    
def test_get_images_with_tag(dynamodb_table, mock_image_and_sentence):
    image, _sentence = mock_image_and_sentence

    image_item = ImageTable.from_model(image)
    image_item.sk = "TAG#foo"
    image_item.save()

    result = get_images_with_tag("foo")
    assert len(result) == 1
    assert result[0].id == image.id


def test_get_sentences_with_tag(dynamodb_table, mock_image_and_sentence):
    _image, sentence = mock_image_and_sentence

    sentence_item = SentenceTable.from_model(sentence)
    sentence_item.pk = sentence_item.sk
    sentence_item.sk = "TAG#foo"
    sentence_item.save()

    result = get_sentences_with_tag("foo")
    assert len(result) == 1
    assert result[0].id == sentence.id


def test_add_tag_to_image(dynamodb_table, mock_image_and_sentence):
    image, _sentence = mock_image_and_sentence

    add_tag_to_image(image, "foo")

    result = get_images_with_tag("foo")
    assert len(result) == 1
    assert result[0].id == image.id


def test_add_tag_to_sentence(dynamodb_table, mock_image_and_sentence):
    _image, sentence = mock_image_and_sentence

    add_tag_to_sentence(sentence, "foo")

    result = get_sentences_with_tag("foo")
    assert len(result) == 1
    assert result[0].id == sentence.id


def test_remove_tag_from_image(dynamodb_table, mock_image_and_sentence):
    image, _sentence = mock_image_and_sentence

    add_tag_to_image(image, "foo")
    remove_tag_from_image(image.id, "foo")

    result = get_images_with_tag("foo")
    assert len(result) == 0


def test_remove_tag_from_sentence(dynamodb_table, mock_image_and_sentence):
    _image, sentence = mock_image_and_sentence

    add_tag_to_sentence(sentence, "foo")
    remove_tag_from_sentence(sentence, "foo")

    result = get_sentences_with_tag("foo")
    assert len(result) == 0
