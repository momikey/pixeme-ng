import pytest
from db.dynamo.image import add_image, add_image_and_lexeme, change_image_visibility, delete_image, get_image_and_lexemes
from db.models.lexeme import Lexeme
from db.models.tables import CoreTableBase, ImageTable, MetaCounterTable

from db.models.image import Image

def test_create_image():
    image = Image(uploader_id=1234)

    assert image is not None
    assert image.id is not None
    assert image.file_url is None


def test_db_add_image(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )

    add_image(image)
    result_image = ImageTable.get(f"IMG#{image.id}", f"IMG#{image.id}")
    result_user = ImageTable.get(f"IMG#{image.id}", f"USER#{image.uploader_id}")
    count = MetaCounterTable.get("IMG##COUNT", "IMG##COUNT")
    assert count.item_count == 1
    assert result_image.user == "user"
    assert result_image.file == "hello.jpg"
    assert result_user.file == "hello.jpg"


def test_db_change_image_visibility(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    change_image_visibility(image, True)
    result_image = ImageTable.get(f"IMG#{image.id}", f"IMG#{image.id}")
    result_user = ImageTable.get(f"IMG#{image.id}", f"USER#{image.uploader_id}")
    assert result_image.private == True
    assert result_user.private == True


def test_db_get_image_and_lexemes(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    result = get_image_and_lexemes(image.id)
    assert result.image.id == image.id
    assert len(result.lexemes) == 0


def test_db_add_image_and_lexeme(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )

    lexeme = Lexeme("foo", "bar")
    add_image_and_lexeme(image, lexeme)

    result = get_image_and_lexemes(image.id)
    assert result.image.id == image.id
    assert len(result.lexemes) == 1
    assert result.lexemes[0].text == "foo"


def test_db_delete_image_and_lexemes(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )

    lexeme = Lexeme("foo", "bar")
    add_image_and_lexeme(image, lexeme)

    delete_image(image.id)
    c = MetaCounterTable.get("IMG##COUNT", "IMG##COUNT")
    assert c.item_count == 0
