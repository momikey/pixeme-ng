import pytest

from db.models.notification import Notification

def test_create_notification():
    notification = Notification(text="test", src="somewhere", user_id=1234)

    assert notification is not None
    assert notification.text is not None
    assert notification.src is not None
