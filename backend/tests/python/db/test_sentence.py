import pytest

from db.dynamo.image import add_image
from db.dynamo.sentence import add_audio_to_sentence, add_sentence_to_image, change_sentence_status, get_audios_for_sentence, get_sentences_for_image, remove_audio_from_sentence, remove_sentence_from_image

from db.models.image import Image
from db.models.lexeme import Lexeme
from db.models.sentence import Audio, Sentence, SentenceStatus
from db.models.tables import AudioTable, LexemeTable, SentenceTable

def test_create_sentence():
    sentence = Sentence(text="This is a test.", language="en", submitter_id=123, image_id="foo")

    assert sentence is not None
    assert sentence.text is not None
    assert sentence.language is not None
    assert len(sentence.audios) == 0


def test_create_audio():
    audio = Audio(uploader_id=1234, language="en", file_url="test", sentence_id="foo")

    assert audio is not None

    sentence = Sentence(text="This is a test.", language="en", submitter_id=123, audios=[audio], image_id="foo")
    assert len(sentence.audios) == 1


def test_get_sentences_for_image(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    sentence_item = SentenceTable.from_model(
        Sentence(image.id, "this is a test", "en", "foo")
    )
    sentence_item.save()

    result = get_sentences_for_image(image.id)
    assert len(result) == 1
    assert result[0].id == sentence_item.id

    result = get_sentences_for_image(image.id, "en")
    assert len(result) == 1

    result = get_sentences_for_image(image.id, "fr")
    assert len(result) == 0


def test_add_sentence_to_image(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    sentence = Sentence(image.id, "this is a test", "en", "foo")
    lexeme = Lexeme("foo", "en")
    sentence.lexemes.append(lexeme)

    add_sentence_to_image(sentence)

    result = get_sentences_for_image(image.id)
    assert len(result) == 1
    assert result[0].id == sentence.id


def test_remove_sentence_from_image(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    sentence = Sentence(image.id, "this is a test", "en", "foo")

    add_sentence_to_image(sentence)
    remove_sentence_from_image(sentence)

    result = get_sentences_for_image(image.id)
    assert len(result) == 0


def test_change_sentence_status(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    sentence = Sentence(image.id, "this is a test", "en", "foo")
    lexeme = Lexeme("foo", "en")
    sentence.lexemes.append(lexeme)

    add_sentence_to_image(sentence)

    change_sentence_status(sentence, SentenceStatus.questioned)

    items = list(SentenceTable.scan())
    assert len(items) == 2
    assert all((item.status == "Q" for item in items))


def test_add_audio_to_sentence(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    sentence = Sentence(image.id, "this is a test", "en", "foo")
    add_sentence_to_image(sentence)

    audio = Audio(uploader_id="user", sentence_id=sentence.id, language="en", file_url="foo.ogg")
    add_audio_to_sentence(audio)

    result = list(AudioTable.query(f"SENT#{sentence.language}#{sentence.id}", AudioTable.sk.startswith("AUD#")))
    assert len(result) == 1
    assert result[0].file == "foo.ogg"


def test_remove_audio_from_sentence(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    sentence = Sentence(image.id, "this is a test", "en", "foo")
    add_sentence_to_image(sentence)

    audio = Audio(uploader_id="user", sentence_id=sentence.id, language="en", file_url="foo.ogg")
    add_audio_to_sentence(audio)
    remove_audio_from_sentence(audio)

    result = list(AudioTable.query(f"SENT#{sentence.language}#{sentence.id}", AudioTable.sk.startswith("AUD#")))
    assert len(result) == 0


def test_get_audios_for_sentence(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    sentence = Sentence(image.id, "this is a test", "en", "foo")
    add_sentence_to_image(sentence)

    audio = Audio(uploader_id="user", sentence_id=sentence.id, language="en", file_url="foo.ogg")
    add_audio_to_sentence(audio)

    result = get_audios_for_sentence(sentence)
    assert len(result) == 1
    assert result[0].file == "foo.ogg"
