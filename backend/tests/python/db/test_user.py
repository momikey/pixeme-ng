import pytest

from db.models.user import User

def test_create_user():
    user = User(name="testuser", email="test@example.com")

    assert user is not None
    assert user.id is not None
    assert isinstance(user, User)
