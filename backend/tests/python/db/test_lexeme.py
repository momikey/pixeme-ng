import pytest
from db.dynamo.image import add_image, add_image_and_lexeme, get_image_and_lexemes
from db.dynamo.lexeme import add_lexeme_to_image, get_lexeme_and_all_data, get_lexeme_and_images, get_lexemes_starting_with_string, get_newest_lexemes, remove_lexeme_from_image

from db.models.image import Image
from db.models.lexeme import Lexeme
from db.models.tables import LexemeTable


def test_create_lexeme():
    lex = Lexeme("test", "en")

    assert lex is not None
    assert lex.lexeme is not None
    assert lex.language is not None
    assert lex.language == "en"


def test_db_get_lexemes_starting_with_string(dynamodb_table):
    lexemes = [Lexeme("foo", "en"), Lexeme("bar", "en"), Lexeme("baz", "en"), Lexeme("bar", "fr")]
    for l in lexemes:
        item = LexemeTable.from_model(l)
        item.save()

    result = get_lexemes_starting_with_string("b", "en")
    assert len(result) == 2
    assert any((r.text == "bar" for r in result))
    assert any((r.text == "baz" for r in result))
    assert all(r.lang == "en" for r in result)


def test_db_get_lexeme_and_images(dynamodb_table):
    image1 = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    image2 = Image(
        uploader_id="other user",
        file_url="something.png"
    )

    lexeme = Lexeme("foo", "bar")
    add_image_and_lexeme(image1, lexeme)
    add_image_and_lexeme(image2, lexeme)

    result = get_lexeme_and_images(lexeme)
    assert result.lexeme.lang == lexeme.language
    assert len(result.images) == 2


def test_db_get_lexeme_and_data(dynamodb_table):
    image1 = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    image2 = Image(
        uploader_id="other user",
        file_url="something.png"
    )

    lexeme = Lexeme("foo", "bar")
    add_image_and_lexeme(image1, lexeme)
    add_image_and_lexeme(image2, lexeme)

    result = get_lexeme_and_all_data(lexeme)
    assert result.lexeme.lang == lexeme.language
    assert len(result.images) == 2


def test_db_add_lexeme_to_image_new(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    lexeme = Lexeme("foo", "bar")
    add_lexeme_to_image(lexeme, image.id)

    result = get_image_and_lexemes(image.id)
    assert len(result.lexemes) == 1


def test_db_add_lexeme_to_image_existing(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    add_image(image)

    lexeme = Lexeme("foo", "bar")
    item = LexemeTable.from_model(lexeme)
    item.save()

    add_lexeme_to_image(lexeme, image.id)

    result = get_image_and_lexemes(image.id)
    assert len(result.lexemes) == 1


def test_db_remove_lexeme_from_image(dynamodb_table):
    image = Image(
        uploader_id="user",
        file_url="hello.jpg"
    )
    lexeme = Lexeme("foo", "bar")

    add_image_and_lexeme(image, lexeme)
    remove_lexeme_from_image(lexeme, image.id)

    result = get_image_and_lexemes(image.id)
    assert len(result.lexemes) == 0


def test_db_get_newest_lexemes(dynamodb_table):
    for i in range(10):
        l = LexemeTable.from_model(Lexeme(f"test-{i}", "test"))
        l.save()

    result = get_newest_lexemes(5)
    assert len(result) == 5
    assert result[0].text == "test-9"
