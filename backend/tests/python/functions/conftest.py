import pytest

from db.dynamo.user import create_user_account
from db.models.user import Profile, User, UserRole

@pytest.fixture()
def mock_user(dynamodb_table) -> str:
    user = User("Test User", "test@example.com", role=UserRole.user)
    user_object = create_user_account(user)

    yield user_object.id
