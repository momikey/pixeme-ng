import pytest

from functions.users.list import list_newest_users

@pytest.fixture
def mock_query_event():
    yield {
        "queryStringParameters": {
            "count": 20
        }
    }

def test_list_newest_users(mock_user, mock_query_event):
    users = list_newest_users(mock_query_event, None)

    assert len(users) == 1
    assert users[0]["id"] == mock_user

