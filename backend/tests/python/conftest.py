import pytest
import os

import boto3
from moto import mock_dynamodb


@pytest.fixture(autouse=True)
def set_environment(monkeypatch):
    monkeypatch.setenv("DYNAMODB_TABLE_NAME", "PixemeTestTable")


@pytest.fixture(autouse=True)
def aws_credentials():
    """Mock AWS credentials for testing."""

    os.environ['AWS_ACCESS_KEY_ID'] = 'testing'
    os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
    os.environ['AWS_SECURITY_TOKEN'] = 'testing'
    os.environ['AWS_SESSION_TOKEN'] = 'testing'
    os.environ['AWS_DEFAULT_REGION'] = 'us-east-1'

@pytest.fixture(autouse=True)
def mock_dynamodb_fixture():
    with mock_dynamodb():
        yield


@pytest.fixture(scope="function")
def dynamodb_resource(aws_credentials):
    with mock_dynamodb():
        yield boto3.resource("dynamodb", region_name="us-east-1")


@pytest.fixture(scope="function")
def dynamodb_table(dynamodb_resource):
    TABLE_NAME = "PixemeTestTable"

    response = dynamodb_resource.create_table(
        TableName=TABLE_NAME,
        AttributeDefinitions=[
            { "AttributeName": "PK", "AttributeType": "S" },
            { "AttributeName": "SK", "AttributeType": "S" },
            # { "AttributeName": "Cls", "AttributeType": "S" },
            { "AttributeName": "ITPK", "AttributeType": "S" },
            { "AttributeName": "Time", "AttributeType": "S" },

        ],
        KeySchema=[
            { "AttributeName": "PK", "KeyType": "HASH" },
            { "AttributeName": "SK", "KeyType": "RANGE" }
        ],
        GlobalSecondaryIndexes=[
            { "IndexName": "InvIdx", "KeySchema": [{ "AttributeName": "SK", "KeyType": "HASH" }, { "AttributeName": "PK", "KeyType": "RANGE" }], "Projection": { "ProjectionType": "ALL"} },
            { "IndexName": "ITimeIdx", "KeySchema": [{ "AttributeName": "ITPK", "KeyType": "HASH" }, { "AttributeName": "Time", "KeyType": "RANGE" }], "Projection": { "ProjectionType": "ALL"} },
        ],
        BillingMode="PROVISIONED",
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        },
    )

    table = dynamodb_resource.Table(TABLE_NAME)
    # from db.models.tables import CoreTableBase

    # table = CoreTableBase.create_table(wait=True)
    
    yield table
