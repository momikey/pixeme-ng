# Lambda function to get a list of users.

from db.dynamo.user import get_newest_users
from db.models.user import User
from cattrs.preconf.json import JsonConverter
from cattrs import Converter

json_converter = JsonConverter()
dict_converter = Converter()

MAX_COUNT = 100

def list_newest_users(event, context):
    """Get a list of the most recent N users, where N is provided by a "count" query parameter (default 20)."""

    count = event.get("queryStringParameters", {}).get("count", 20)

    if count > MAX_COUNT:
        # TODO: Error condition
        pass

    result = get_newest_users(count)

    if result:
        users = [r.to_model() for r in result]
        return dict_converter.unstructure(users)
