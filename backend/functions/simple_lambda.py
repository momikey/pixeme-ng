from db.models.user import User

def handler(event, context):
  return {
    "statusCode": 200,
    "body": "Hello, World! Your request was received at {}.".format(event['requestContext']['time'])
  }
