from attrs import (define, Factory, field)

@define
class Lexeme:
    """A lexeme and its associated language code."""

    lexeme: str
    language: str
