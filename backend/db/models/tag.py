from enum import Enum
from attrs import (define, Factory, field)


@define
class Tag:
    """A Pixeme tag, which can be attached to an image or a sentence."""

    text: str
