from datetime import datetime
from typing import List, Optional
from attrs import (define, Factory, field)
from hashids import Hashids
from simpleflake import simpleflake
from . import ID_EPOCH

SALT = "notification"

ID_GENERATOR = Hashids(min_length=6, salt=SALT)


@define
class Notification:
    """A user notification"""

    text: str
    src: str
    user_id: int
    id: str = field()
    link_url: Optional[str] = None
    read: bool = False
    time_created: datetime = datetime.now()

    @id.default
    def _create_id(self):
        return ID_GENERATOR.encode(simpleflake(epoch=ID_EPOCH))

    def id_to_hash(self):
        return ID_GENERATOR.encode(self.id)

    def id_from_hash(self, hash: str):
        return ID_GENERATOR.decode(hash)[0]