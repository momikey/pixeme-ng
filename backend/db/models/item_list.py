from datetime import datetime
from enum import Enum
from typing import Set
from attrs import (define, Factory, field)
from hashids import Hashids
from simpleflake import simpleflake
from . import ID_EPOCH

SALT = "list"

ID_GENERATOR = Hashids(min_length=6, salt=SALT)

class ListType(str, Enum):
    """A list type."""

    images = "I"
    sentences = "S"
    lexemes = "L"


@define
class ItemList:
    """A list of items created by a user"""

    name: str
    creator_id: str
    contents: Set[str]
    type: ListType = field(converter=ListType)
    id: str = field()
    last_updated: datetime = datetime.now()
    
    @id.default
    def _create_id(self):
        return ID_GENERATOR.encode(simpleflake(epoch=ID_EPOCH))

    def id_to_hash(self):
        return ID_GENERATOR.encode(self.id)

    def id_from_hash(self, hash: str):
        return ID_GENERATOR.decode(hash)[0]
