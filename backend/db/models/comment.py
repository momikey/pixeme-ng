from datetime import datetime
from enum import Enum
from attrs import (define, Factory, field)
from hashids import Hashids
from simpleflake import simpleflake
from . import ID_EPOCH

SALT = "comment"

ID_GENERATOR = Hashids(min_length=6, salt=SALT)


class CommentType(str, Enum):
    """A comment type"""

    image = "I"
    sentence = "S"
    audio = "A"


class CommentStatus(str, Enum):
    """The status of a comment"""

    public = "P"
    draft = "D"
    held = "H"
    deleted = "X"


@define
class Comment:
    """A user comment"""

    text: str
    time_created: datetime
    user_id: str
    type: CommentType
    parent_reference: str
    id: str = field()
    status: CommentStatus = CommentStatus.draft

    @id.default
    def _create_id(self):
        return ID_GENERATOR.encode(simpleflake(epoch=ID_EPOCH))

    def id_to_hash(self):
        return ID_GENERATOR.encode(self.id)

    def id_from_hash(self, hash: str):
        return ID_GENERATOR.decode(hash)[0]
