# Definitions for the DynamoDB tables

from datetime import datetime
import os
import profile
from typing import List

from attrs import define, field

from pynamodb.models import Model
from pynamodb.attributes import DiscriminatorAttribute, UnicodeAttribute, NumberAttribute, UTCDateTimeAttribute, DynamicMapAttribute, BooleanAttribute, MapAttribute, UnicodeSetAttribute
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from db.models.comment import Comment
from db.models.image import Image
from db.models.item_list import ItemList
from db.models.language import Language

from db.models.lexeme import Lexeme
from db.models.notification import Notification
from db.models.sentence import Audio, Sentence
from db.models.tag import Tag
from db.models.user import Fluency, Profile, User


class ProfileAttributes(DynamicMapAttribute):
    """A custom attribute class for profile data."""

    # Add attributes as we need them.


class InvertedIndex(GlobalSecondaryIndex):
    """Inverted index, used to fetch items using the sort key as the primary key and vice versa."""

    class Meta:
        index_name = "InvIdx"
        read_capacity_units = 5
        write_capacity_units = 1
        projection = AllProjection()

    pk = UnicodeAttribute(hash_key=True, attr_name="SK")
    sk = UnicodeAttribute(range_key=True, attr_name="PK")


class TimestampIndex(GlobalSecondaryIndex):
    """Sparse index for querying by insertion timestamp."""

    class Meta:
        index_name = "ITimeIdx"
        read_capacity_units = 5
        write_capacity_units = 1
        projection = AllProjection()

    itpk = UnicodeAttribute(hash_key=True, attr_name="ITPK")
    time = UTCDateTimeAttribute(range_key=True, attr_name="Time")


class CoreTableBase(Model):
    """Base definition for the table. Individual object types are represented as subclasses."""

    class Meta:
        table_name = os.getenv("DYNAMODB_TABLE_NAME", "PixemeTestTable")
    
    # All objects will have a primary and sort key.
    pk = UnicodeAttribute(hash_key=True, attr_name="PK")
    sk = UnicodeAttribute(range_key=True, attr_name="SK")

    # Pynamodb uses a discriminator attribute to determine what class an object is.
    cls = DiscriminatorAttribute(attr_name="Cls")

    # Most objects will have timestamps for last update. Those that do will need a primary key for the timestamp index.
    time = UTCDateTimeAttribute(attr_name="Time", default=datetime.now)
    itpk = UnicodeAttribute(attr_name="ITPK", null=True)

    # All objects can use the inverted index.
    inverted_index = InvertedIndex()

    # The timestamp index is a sparse index, but it has to be defined on the base class.
    timestamp_index = TimestampIndex()


class IdMixin:
    """Mixin class for objects that use a Flake ID."""
    id = UnicodeAttribute(attr_name="Id")


class TextMixin:
    """Mixin class for objects that have a general text field."""
    text = UnicodeAttribute(attr_name="Text")


class LangMixin:
    """Mixin class for objects that use a language code attribute."""
    lang = UnicodeAttribute(attr_name="Lang")


class UserMixin:
    """Mixin class for objects that are connected to a user in some way. The semantics are not important here."""
    user = UnicodeAttribute(attr_name="User")


class ImageTable(CoreTableBase, UserMixin, IdMixin, discriminator="IM"):
    """Schema for an image object."""

    file = UnicodeAttribute(attr_name="File")
    private = BooleanAttribute(default=False)
    meta = MapAttribute(attr_name="Meta", default={})
    itpk = UnicodeAttribute(attr_name="ITPK", default="IMG", null=True)

    # Nullable properties to allow querying through the inverted index.
    lang = UnicodeAttribute(attr_name="Lang", null=True)
    text = UnicodeAttribute(attr_name="Text", null=True)

    @classmethod
    def from_model(cls, model: Image):
        return cls(
            f"IMG#{model.id}",
            f"IMG#{model.id}",
            id=model.id,
            user=model.uploader_id,
            file=model.file_url,
            private=model.private
        )

    def to_model(self) -> Image:
        return Image(id=self.id, uploader_id=self.user, file_url=self.file, private=self.private)


class LexemeTable(CoreTableBase, TextMixin, LangMixin, discriminator="LX"):
    """Schema for a lexeme object."""
    
    itpk = UnicodeAttribute(attr_name="ITPK", default="LEX", null=True)

    @classmethod
    def from_model(cls, model: Lexeme):
        return cls(f"LEX#{model.language}", f"LEX#{model.language}#{model.lexeme}", text=model.lexeme, lang=model.language)

    def to_model(self) -> Lexeme:
        return Lexeme(self.text, self.lang)


class SentenceTable(CoreTableBase, TextMixin, LangMixin, UserMixin, IdMixin, discriminator="SE"):
    """Schema for a sentence object."""

    itpk = UnicodeAttribute(attr_name="ITPK", default="SENT", null=True)
    image = UnicodeAttribute(attr_name="ImId")
    status = UnicodeAttribute(attr_name="Status")

    @classmethod
    def from_model(cls, model: Sentence):
        return cls(
            f"IMG#{model.image_id}",
            f"SENT#{model.language}#{model.id}",
            id=model.id,
            image=model.image_id,
            user=model.submitter_id,
            lang=model.language,
            text=model.text,
            status=model.status.value
        )

    def to_model(self) -> Sentence:
        return Sentence(
            id = self.id,
            image_id=self.image,
            submitter_id=self.user,
            language=self.lang,
            text=self.text,
            status=self.status
        )


class AudioTable(CoreTableBase, UserMixin, LangMixin, IdMixin, discriminator="AU"):
    """Schema for an audio object."""

    file = UnicodeAttribute(attr_name="File")
    meta = MapAttribute(attr_name="Meta", default={})
    sentence = UnicodeAttribute(attr_name="SenId")
    itpk = UnicodeAttribute(attr_name="ITPK", default="AUD", null=True)

    @classmethod
    def from_model(cls, model: Audio):
        return cls(
            f"SENT#{model.language}#{model.sentence_id}",
            f"AUD#{model.id}",
            id=model.id,
            user=model.uploader_id,
            sentence=model.sentence_id,
            lang=model.language,
            file=model.file_url
        )

    def to_model(self) -> Audio:
        return Audio(id=self.id, language=self.lang, uploader_id=self.user, file_url=self.file, sentence_id=self.sentence)

class TagTable(CoreTableBase, TextMixin, discriminator="TG"):
    """Schema for a tag object."""

    itpk = UnicodeAttribute(attr_name="ITPK", default="TAG", null=True)

    @classmethod
    def from_model(cls, model: Tag):
        return cls("##TAG", f"TAG#{model.text}", text=model.text)

    def to_model(self) -> Tag:
        return Tag(text=self.text)
        
class UserTable(CoreTableBase, IdMixin, discriminator="US"):
    """Schema for a user object."""

    name = UnicodeAttribute(attr_name="Name")
    email = UnicodeAttribute(attr_name="Email")
    role = NumberAttribute(attr_name="Role")
    itpk = UnicodeAttribute(attr_name="ITPK", default="USER", null=True)

    @classmethod
    def from_model(cls, model: User):
        return cls(
            f"USER#{model.id}",
            f"USER#{model.id}",
            id=model.id,
            name=model.name,
            email=model.email,
            role=model.role.value
        )

    def to_model(self) -> User:
        return User(name=self.name, email=self.email, id=self.id, role=self.role)


class NotificationTable(CoreTableBase, TextMixin, IdMixin, UserMixin, discriminator="NO"):
    """Schema for a notification object."""

    src = UnicodeAttribute(attr_name="Src")
    link = UnicodeAttribute(attr_name="Link")
    read = BooleanAttribute(attr_name="Read")
    itpk = UnicodeAttribute(attr_name="ITPK", default="MSG", null=True)

    @classmethod
    def from_model(cls, model: Notification):
        return cls(
            f"USER#{model.user_id}",
            f"MSG#{model.id}",
            id=model.id,
            text=model.text,
            src=model.src,
            link=model.link_url,
            read=model.read
        )

    def to_model(self) -> Notification:
        return Notification(
            id=self.id,
            text=self.text,
            src=self.src,
            link_url=self.link,
            user_id=self.user,
            read=self.read,
            time_created=self.time
        )


class ListTable(CoreTableBase, UserMixin, IdMixin, discriminator="LI"):
    """Schema for a list object."""

    name = UnicodeAttribute(attr_name="Name")
    updated = UTCDateTimeAttribute(attr_name="Updated")
    list_type = UnicodeAttribute(attr_name="Type")
    contents = UnicodeSetAttribute(attr_name="Contents")
    itpk = UnicodeAttribute(attr_name="ITPK", default="LIST", null=True)

    @classmethod
    def from_model(cls, model: ItemList):
        return cls(
            f"USER#{model.creator_id}",
            f"LIST#{model.creator_id}#{model.id}",
            id=model.id,
            user=model.creator_id,
            name=model.name,
            updated=model.last_updated,
            list_type=model.type.value,
            contents=model.contents
        )

    def to_model(self) -> ItemList:
        return ItemList(
            id=self.id,
            creator_id=self.user,
            name=self.name,
            type=self.list_type,
            contents=self.contents,
            last_updated=self.updated
        )


class LanguageTable(CoreTableBase, LangMixin, discriminator="LN"):
    """Schema for a language object."""

    name =UnicodeAttribute(attr_name="Name")
    native = UnicodeAttribute(attr_name="Native")
    itpk = UnicodeAttribute(attr_name="ITPK", default="LANG", null=True)

    @classmethod
    def from_model(cls, model: Language):
        return cls("##LANG", "LANG#{model.code}", lang=model.code, name=model.name, native=model.native)

    def to_mmdel(self) -> Language:
        return Language(code=self.lang, name=self.name, native=self.native)


class CommentTable(CoreTableBase, UserMixin, TextMixin, IdMixin, discriminator="CM"):
    """Schema for a comment object."""

    comment_type = UnicodeAttribute(attr_name="Type")
    parent_id = UnicodeAttribute(attr_name="Ref")
    status = UnicodeAttribute(attr_name="Status")
    itpk = UnicodeAttribute(attr_name="ITPK", default="COMM", null=True)

    @classmethod
    def from_model(cls, model: Comment):
        return cls(
            f"USER#{model.user_id}",
            f"COMM#{model.user_id}#{model.id}",
            id=model.id,
            user=model.user_id,
            text=model.text,
            parent_id=model.parent_reference,
            status=model.status.value,
            comment_type=model.type.value
        )

    def to_model(self) -> Comment:
        return Comment(
            id=self.id,
            user_id=self.user,
            parent_reference=self.parent_id,
            text=self.text,
            status=self.status,
            type=self.comment_type,
            time_created=self.time
        )


class FluencyTable(CoreTableBase, UserMixin, LangMixin, discriminator="FL"):
    """Schema for a fluency object."""

    level = NumberAttribute(attr_name="Level")

    @classmethod
    def from_model(cls, model: Fluency):
        return cls(f"USER#{model.user_id}", f"LANG#{model.language}", user=model.user_id, lang=model.language, level=model.level)

    def to_model(self) -> Fluency:
        return Fluency(user_id=self.user, language=self.lang, level=self.level)


class ProfileTable(CoreTableBase, UserMixin, discriminator="PR"):
    """Schema for a user profile object."""

    updated = UTCDateTimeAttribute(attr_name="Updated")
    profile = ProfileAttributes(null=True)
    itpk = UnicodeAttribute(attr_name="ITPK", default="PROF", null=True)

    @classmethod
    def from_model(cls, model: Profile):
        return cls(f"USER#{model.user_id}", f"USER#{model.user_id}#PROF", updated=model.last_updated, profile=model.profile_data)

    def to_model(self) -> Profile:
        return Profile(user_id=self.user, profile_data=self.profile.as_dict(), last_updated=self.updated)


class MetaCounterTable(CoreTableBase, discriminator="C"):
    """Schema for meta objects that serve as counters."""

    # No need to convert to/from models here.
    item_count = NumberAttribute(attr_name="Count")


# Data structures for query results.

@define
class ImageAndLexemes:
    """An image and its associated lexemes."""

    image: ImageTable = field()
    lexemes: List[LexemeTable] = field(factory=list)


@define
class LexemeAndImages:
    """A lexeme and all images that have it associated with them."""

    lexeme: LexemeTable = field()
    images: List[ImageTable] = field(factory=list)


@define
class LexemeAndData:
    """A lexeme and all data (images and sentences) that have it assocated with them."""

    lexeme: LexemeTable = field()
    images: List[ImageTable] = field(factory=list)
    sentences: List[SentenceTable] = field(factory=list)


@define
class UserAndProfile:
    """A user and associated profile."""

    user: UserTable
    profile: ProfileTable
