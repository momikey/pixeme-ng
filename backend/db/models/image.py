from typing import List, Optional
from attrs import (define, Factory, field)
from hashids import Hashids
from simpleflake import simpleflake

from .sentence import Sentence
from .lexeme import Lexeme
from . import ID_EPOCH

SALT = "image"

ID_GENERATOR = Hashids(min_length=6, salt=SALT)


@define
class Image:
    """A Pixeme image and associated metadata."""

    uploader_id: str
    id: str = field()
    file_url: Optional[str] = None
    sentences: List[Sentence] = field(factory=list)
    lexemes: List[Lexeme] = field(factory=list)
    private: bool = False

    @id.default
    def _create_id(self):
        return ID_GENERATOR.encode(simpleflake(epoch=ID_EPOCH))

    def id_to_hash(self):
        return ID_GENERATOR.encode(self.id)

    def id_from_hash(self, hash: str):
        return ID_GENERATOR.decode(hash)[0]
