from enum import Enum
from typing import List, Optional
from attrs import (define, Factory, field)
from hashids import Hashids
from simpleflake import simpleflake

from db.models.lexeme import Lexeme
from . import ID_EPOCH

AUDIO_SALT = "audio"
AUDIO_ID_GENERATOR = Hashids(min_length=6, salt=AUDIO_SALT)

SENTENCE_SALT = "sentence"
SENTENCE_ID_GENERATOR = Hashids(min_length=6, salt=SENTENCE_SALT)


class SentenceStatus(str, Enum):
    """Status for a sentence."""

    waiting = "W"
    approved = "A"
    rejected = "R"
    questioned = "Q"


@define
class Audio:
    """An audio for a sentence in Pixeme."""

    uploader_id: str
    sentence_id: str
    language: str
    id: str = field()
    file_url: Optional[str] = None
    
    @id.default
    def _create_id(self):
        return AUDIO_ID_GENERATOR.encode(simpleflake(epoch=ID_EPOCH))

    def id_to_hash(self):
        return AUDIO_ID_GENERATOR.encode(self.id)

    def id_from_hash(self, hash: str):
        return AUDIO_ID_GENERATOR.decode(hash)[0]


@define
class Sentence:
    """A Pixeme sentence to be attached to an image."""

    image_id: str
    text: str
    language: str
    submitter_id: str
    id: str = field()
    status: SentenceStatus = field(converter=SentenceStatus, default=SentenceStatus.waiting)
    audios: List[Audio] = field(factory=list)
    lexemes: List[Lexeme] = field(factory=list)
    
    @id.default
    def _create_id(self):
        return SENTENCE_ID_GENERATOR.encode(simpleflake(epoch=ID_EPOCH))

    def id_to_hash(self):
        return SENTENCE_ID_GENERATOR.encode(self.id)

    def id_from_hash(self, hash: str):
        return SENTENCE_ID_GENERATOR.decode(hash)[0]

