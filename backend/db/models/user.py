from datetime import datetime
from enum import Enum
from functools import total_ordering
from typing import Any, Dict, List, Optional
from attrs import (define, Factory, field)
import attrs.validators
from hashids import Hashids
from simpleflake import simpleflake
from . import ID_EPOCH

SALT = "user"

ID_GENERATOR = Hashids(min_length=6, salt=SALT)


@total_ordering
class UserRole(Enum):
    """User role/privilege level."""

    # For inactive, banned, or guest users
    not_user = 0

    # Normal users
    user = 1

    # Users with upgraded privileges
    privileged_user = 3

    # Moderator-level users
    moderator = 5

    # Administrator-level users
    admin = 9

    # The usual code to make this an ordered enum.
    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented


@define
class Profile:
    """Profile information for a Pixeme user"""

    user_id: str
    last_updated: datetime
    profile_data: Dict[str, Any]


@define
class Fluency:
    """Language fluency for a Pixeme user"""

    user_id: str
    language: str
    level: int = field(validator=attrs.validators.in_([1,2,3,4,5]))


@define
class User:
    """A Pixeme user"""

    name: str
    email: str
    id: str = field()
    fluencies: List[Fluency] = field(factory=list)
    profile: Optional[Profile] = None
    role: UserRole = field(default=UserRole.not_user, converter=UserRole)

    @id.default
    def _create_id(self):
        return ID_GENERATOR.encode(simpleflake(epoch=ID_EPOCH))

    def id_to_hash(self):
        return ID_GENERATOR.encode(self.id)

    def id_from_hash(self, hash: str):
        return ID_GENERATOR.decode(hash)[0]
