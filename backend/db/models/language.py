from typing import Optional
from attrs import (define, Factory, field)

@define
class Language:
    """A language, as defined by its RFC 5646 language code."""

    code: str
    name: str
    native: Optional[str]
