# DynamoDB access patterns for tags

from typing import List
from pynamodb.connection import Connection
from pynamodb.transactions import TransactWrite
from db.models.sentence import Sentence

from db.models.tables import ImageAndLexemes, ImageTable, InvertedIndex, SentenceTable, TagTable
from db.models.tag import Tag
from ..models.image import Image


def add_tag_to_image(image: Image, tag: str) -> ImageTable:
    """Add a tag to an image."""

    tag_item = TagTable.from_model(Tag(tag))
    tagged_image = ImageTable.from_model(image)
    tagged_image.sk = tag_item.sk
    tagged_image.itpk = None

    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        transaction.save(tag_item)
        transaction.save(tagged_image)

    return tagged_image


def add_tag_to_sentence(sentence: Sentence, tag: str) -> SentenceTable:
    """Add a tag to a sentence."""

    tag_item = TagTable.from_model(Tag(tag))
    tagged_sentence = SentenceTable.from_model(sentence)
    tagged_sentence.pk = tagged_sentence.sk
    tagged_sentence.sk = tag_item.sk
    tagged_sentence.itpk = None

    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        transaction.save(tag_item)
        transaction.save(tagged_sentence)

    return tagged_sentence


def remove_tag_from_image(image_id: str, tag: str) -> None:
    """Remove a tag from an image."""

    image_item = ImageTable(f"IMG#{image_id}", f"TAG#{tag}")
    image_item.delete()


def remove_tag_from_sentence(sentence: Sentence, tag: str) -> None:
    """Remove a tag from a sentence."""

    image_item = SentenceTable(f"SENT#{sentence.language}#{sentence.id}", f"TAG#{tag}")
    image_item.delete()


def get_images_with_tag(tag: str) -> List[ImageTable]:
    """Get all images with the given tag."""

    items = ImageTable.inverted_index.query(f"TAG#{tag}", InvertedIndex.sk.startswith("IMG#"))
    return list(items)


def get_sentences_with_tag(tag: str) -> List[ImageTable]:
    """Get all sentences with the given tag."""

    items = SentenceTable.inverted_index.query(f"TAG#{tag}", InvertedIndex.sk.startswith("SENT#"))
    return list(items)

