# DynamoDB access patterns for images

from pynamodb.connection import Connection
from pynamodb.transactions import TransactWrite

from db.models.tables import ImageAndLexemes, ImageTable, LexemeTable, MetaCounterTable
from ..models.image import Image
from ..models.lexeme import Lexeme

def add_image(image: Image) -> ImageTable:
    """Add an image without an associated lexeme."""

    image_item = ImageTable.from_model(image)
    # Copies of the image with different sort keys for easy searching.
    image_user = ImageTable(image_item.pk, f"USER#{image.uploader_id}", user=image_item.user, id=image_item.id, file=image_item.file, private=image_item.private, itpk=None)
    image_count = MetaCounterTable("IMG##COUNT", "IMG##COUNT")

    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        transaction.save(image_item, condition=(ImageTable.pk.does_not_exist() & ImageTable.sk.does_not_exist()))
        transaction.save(image_user, condition=(ImageTable.pk.does_not_exist() & ImageTable.sk.does_not_exist()))
        transaction.update(
            image_count,
            actions=[MetaCounterTable.item_count.add(1)]
        )

    return image_item


def add_image_and_lexeme(image: Image, lexeme: Lexeme) -> ImageTable:
    """Add an image with an initial associated lexeme."""

    image_item = ImageTable.from_model(image)
    lexeme_item = LexemeTable.from_model(lexeme)
    # Copies of items with different sort keys for easy searching.
    image_user = ImageTable(image_item.pk, f"USER#{image.uploader_id}", user=image_item.user, id=image_item.id, file=image_item.file, private=image_item.private, itpk=None)
    image_lexeme = ImageTable(image_item.pk, f"LEX#{lexeme.language}#{lexeme.lexeme}", user=image_item.user, id=image_item.id, file=image_item.file, private=image_item.private, lang=lexeme_item.lang, text=lexeme_item.text, itpk=None)
    image_count = MetaCounterTable("IMG##COUNT", "IMG##COUNT")

    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        transaction.save(image_item, condition=(ImageTable.pk.does_not_exist() & ImageTable.sk.does_not_exist()))
        transaction.save(image_user, condition=(ImageTable.pk.does_not_exist() & ImageTable.sk.does_not_exist()))
        transaction.save(image_lexeme, condition=(ImageTable.pk.does_not_exist() & ImageTable.sk.does_not_exist()))
        transaction.update(
            image_count,
            actions=[MetaCounterTable.item_count.add(1)]
        )
    lexeme_item.save()

    return image_item


def change_image_visibility(image: Image, visibility: bool) -> ImageTable:
    """Change an image's visibility status."""

    image_item = ImageTable.from_model(image)
    other_images = list(ImageTable.query(image_item.pk))
    
    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        for l in other_images:
            transaction.update(l, actions=[ImageTable.private.set(visibility)])
    
    return image_item


def get_image_and_lexemes(image_id: str) -> ImageAndLexemes:
    """Get an image and all its associated lexemes."""

    image_item = ImageTable.get(f"IMG#{image_id}", f"IMG#{image_id}")
    lexeme_items = ImageTable.query(f"IMG#{image_id}", LexemeTable.sk.startswith("LEX#"))

    return ImageAndLexemes(image_item, list(lexeme_items))


def delete_image(image_id: str) -> None:
    """Remove an image and all its associated data (lexemes, sentences, etc.) from the database."""

    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        transaction.delete(ImageTable(f"IMG#{image_id}", f"IMG#{image_id}"), condition=(ImageTable.pk.exists() & ImageTable.sk.exists()))
        transaction.update(
            MetaCounterTable("IMG##COUNT", "IMG##COUNT"),
            actions=[MetaCounterTable.item_count.add(-1)]
        )
    with ImageTable.batch_write() as batch:
        image_items = ImageTable.query(f"IMG#{image_id}")
        for i in image_items:
            batch.delete(i)
    with LexemeTable.batch_write() as batch:
        lexeme_items = LexemeTable.query(f"IMG#{image_id}", LexemeTable.sk.startswith("LEX#"))
        for l in lexeme_items:
            batch.delete(l)
