# DynamoDB access functions for comments

from typing import List

from pynamodb.connection import Connection
from pynamodb.transactions import TransactWrite

from db.models.comment import Comment, CommentStatus, CommentType
from db.models.tables import CommentTable


def create_comment(comment: Comment) -> CommentTable:
    """Create a comment of any type."""

    user_comment_item = CommentTable.from_model(comment)
    typed_comment_item = user_comment_item
    if comment.type == CommentType.audio:
        typed_comment_item.pk = f"AUD#{comment.parent_reference}"
    elif comment.type == CommentType.image:
        typed_comment_item.pk = f"IMG#{comment.parent_reference}"
    elif comment.type == CommentType.sentence:
        typed_comment_item.pk = f"SENT#{comment.parent_reference}"

    with CommentTable.batch_write() as batch:
        batch.save(user_comment_item)
        batch.save(typed_comment_item)

    return user_comment_item


def change_comment_status(comment: Comment, new_status: CommentStatus) -> CommentTable:
    """Change the status of a comment of any type."""

    user_comment_item = CommentTable.from_model(comment)
    typed_comment_item = user_comment_item
    if comment.type == CommentType.audio:
        typed_comment_item.pk = f"AUD#{comment.parent_reference}"
    elif comment.type == CommentType.image:
        typed_comment_item.pk = f"IMG#{comment.parent_reference}"
    elif comment.type == CommentType.sentence:
        typed_comment_item.pk = f"SENT#{comment.parent_reference}"

    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        user_comment_item.update(actions=[CommentTable.status.set(new_status.value)])
        typed_comment_item.update(actions=[CommentTable.status.set(new_status.value)])

    user_comment_item.status = new_status.value
    return user_comment_item


def get_comments_of_typed_object(object_id: str, comment_type: CommentType) -> List[CommentTable]:
    """Get all the comments of an object of a specific type."""

    pk_types = { CommentType.audio: "AUD#", CommentType.image: "IMG#", CommentType.sentence: "SENT#" }

    pk = pk_types[comment_type] + object_id
    comment_items = CommentTable.query(pk, CommentTable.sk.startswith("COMM#"))

    return list(comment_items)


def get_comments_for_user(user_id: str) -> List[CommentTable]:
    """Get all comments posted by a specific user."""

    comment_items = CommentTable.query(f"USER#{user_id}", CommentTable.sk.startswith("COMM#"))
    return list(comment_items)


def delete_comment_of_type(comment: Comment) -> None:
    """Delete a comment. This is a hard delete that completely removes the comment from the database."""

    user_comment_item = CommentTable.from_model(comment)
    typed_comment_item = user_comment_item
    if comment.type == CommentType.audio:
        typed_comment_item.pk = f"AUD#{comment.parent_reference}"
    elif comment.type == CommentType.image:
        typed_comment_item.pk = f"IMG#{comment.parent_reference}"
    elif comment.type == CommentType.sentence:
        typed_comment_item.pk = f"SENT#{comment.parent_reference}"

    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        user_comment_item.delete()
        typed_comment_item.delete()
