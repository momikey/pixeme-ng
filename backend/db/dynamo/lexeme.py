# DynamoDB access functions for lexemes

from typing import List

from db.models.tables import CoreTableBase, ImageTable, InvertedIndex, LexemeAndData, LexemeAndImages, LexemeTable, MetaCounterTable, SentenceTable
from ..models.lexeme import Lexeme


def get_lexemes_starting_with_string(prefix: str, language: str) -> List[LexemeTable]:
    """Get all the lexemes from a specific language that start with a string."""

    lexeme_items = LexemeTable.query(f"LEX#{language}", LexemeTable.sk.startswith(f"LEX#{language}#{prefix}"))

    return list(lexeme_items)


def get_lexeme_and_images(lexeme: Lexeme) -> LexemeAndImages:
    """Get a lexeme and all images it has been associated with."""

    lexeme_item = LexemeTable.get(f"LEX#{lexeme.language}", f"LEX#{lexeme.language}#{lexeme.lexeme}")
    image_items = ImageTable.inverted_index.query(f"LEX#{lexeme.language}#{lexeme.lexeme}", InvertedIndex.sk.startswith(f"IMG#"))

    return LexemeAndImages(lexeme=lexeme_item, images=list(image_items))


def get_lexeme_and_all_data(lexeme: Lexeme) -> LexemeAndData:
    """Get a lexeme and all associated data."""

    all_items = CoreTableBase.inverted_index.query(f"LEX#{lexeme.language}#{lexeme.lexeme}")

    # Is there a better way to do this, I wonder?
    result = LexemeAndData("")
    for i in all_items:
        if isinstance(i, LexemeTable):
            result.lexeme = i
        elif isinstance(i, ImageTable):
            result.images.append(i)
        elif isinstance(i, SentenceTable):
            result.sentences.append(i)
    return result


def add_lexeme_to_image(lexeme: Lexeme, image_id: str) -> LexemeTable:
    """Add a new lexeme to an image."""

    lexeme_item = LexemeTable.from_model(lexeme)

    image_item = ImageTable.get(f"IMG#{image_id}", f"IMG#{image_id}")
    image_lexeme_item = image_item
    image_lexeme_item.sk = lexeme_item.sk
    image_lexeme_item.lang = lexeme_item.lang
    image_lexeme_item.text = lexeme_item.text

    image_lexeme_item.save(condition=(ImageTable.pk.does_not_exist() & ImageTable.sk.does_not_exist()))
    lexeme_item.save()

    return lexeme_item


def remove_lexeme_from_image(lexeme: Lexeme, image_id: str) -> None:
    """Remove a lexeme from an image."""

    image_lexeme_item = ImageTable.get(f"IMG#{image_id}", f"LEX#{lexeme.language}#{lexeme.lexeme}")
    image_lexeme_item.delete()


def get_newest_lexemes(count: int) -> List[LexemeTable]:
    """Get the newest N lexemes."""

    result = LexemeTable.timestamp_index.query("LEX", scan_index_forward=False, limit=count)

    return list(result)
