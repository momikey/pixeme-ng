# DynamoDB access functions for item lists

from typing import Union

from db.models.image import Image
from db.models.item_list import ItemList
from db.models.lexeme import Lexeme
from db.models.sentence import Sentence
from db.models.tables import ListTable

# Note that we refer to "objects" instead of "items" in this module,
# because talking about an "item list item" is just too confusing for me.

def create_item_list(item_list: ItemList) -> ListTable:
    """Create a new item list."""

    item_list_object = ListTable.from_model(item_list)
    item_list_object.save()

    return item_list_object


def delete_item_list(item_list: ItemList) -> None:
    """Delete an item list."""

    item_list_object = ListTable.from_model(item_list)
    item_list_object.delete(conndition=(ListTable.pk.exists() & ListTable.sk.exists()))


def add_item_to_item_list(item_list: ItemList, item: Union[Image, Sentence, Lexeme]) -> ListTable:
    """Add a new item to a list."""

    item_list_object = ListTable.from_model(item_list)
    new_item_key = item.id if not isinstance(item, Lexeme) else (item.language, item.lexeme).join("#")
    item_list_object.update(actions=[ListTable.contents.add(new_item_key)])
    
    return item_list_object


def remove_item_from_item_list(item_list: ItemList, key: str) -> ListTable:
    """Remove an item from a list."""

    item_list_object = ListTable.from_model(item_list)
    item_list_object.update(actions=[ListTable.contents.remove(key)])

    return item_list_object
