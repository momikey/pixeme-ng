# DynamoDB access functions for sentences

from genericpath import exists
from typing import List, Optional

from pynamodb.connection import Connection
from pynamodb.transactions import TransactWrite

from db.models.tables import AudioTable, CoreTableBase, ImageTable, InvertedIndex, LexemeAndData, LexemeAndImages, LexemeTable, MetaCounterTable, SentenceTable
from ..models.sentence import Audio, Sentence, SentenceStatus


def get_sentences_for_image(image_id: str, language: Optional[str] = None) -> List[SentenceTable]:
    """Get all the sentences for an image."""

    sort_key = "SENT#"
    if language is not None:
        sort_key += f"{language}#"
    sentence_items = SentenceTable.query(f"IMG#{image_id}", SentenceTable.sk.startswith(sort_key))

    return list(sentence_items)


def add_sentence_to_image(sentence: Sentence) -> SentenceTable:
    """Add a new sentence to an image."""

    sentence_item = SentenceTable.from_model(sentence)
    sentence_item.save()
    with SentenceTable.batch_write() as batch:
        for l in sentence.lexemes:
            item = LexemeTable.from_model(l)
            sentence_item.pk = sentence_item.sk
            sentence_item.sk = item.pk
            sentence_item.itpk = None
            batch.save(sentence_item)

    return sentence_item


def remove_sentence_from_image(sentence: Sentence) -> None:
    """Remove a sentence from an image. This also removes all audios associated with that sentence."""

    sentence_item = SentenceTable.from_model(sentence)

    sentence_item.delete(condition=(SentenceTable.pk.exists() & SentenceTable.sk.exists()))
    with AudioTable.batch_write() as batch:
        audio_items = AudioTable.query(sentence_item.sk, AudioTable.sk.startswith("AUD#"))
        for a in audio_items:
            batch.delete(a)


def change_sentence_status(sentence: Sentence, new_status: SentenceStatus) -> SentenceTable:
    """Change a sentence's validation status."""

    sentence_item = SentenceTable.from_model(sentence)
    sentence_item.update(actions=[SentenceTable.status.set(new_status)], condition=(SentenceTable.pk.exists() & SentenceTable.sk.exists()))

    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        lexeme_sentence_items = SentenceTable.query(sentence_item.sk, SentenceTable.sk.startswith(f"LEX#{sentence.language}"))
        for l in lexeme_sentence_items:
            transaction.update(l, actions=[SentenceTable.status.set(new_status)])

    return sentence_item


def add_audio_to_sentence(audio: Audio) -> AudioTable:
    """Add an audio to a sentence."""

    # The sentence ID is stored as part of the audio.
    audio_item = AudioTable.from_model(audio)
    audio_item.save()

    return audio_item


def remove_audio_from_sentence(audio: Audio) -> None:
    """Remove an audio from a sentence."""

    audio_item = AudioTable.from_model(audio)
    audio_item.delete()


def get_audios_for_sentence(sentence: Sentence) -> List[AudioTable]:
    """Get a list of audios associated with this sentence."""

    audio_items = AudioTable.query(f"SENT#{sentence.language}#{sentence.id}", AudioTable.sk.startswith("AUD#"))
    return list(audio_items)
