# DynamoDB access functions for users

from datetime import datetime
from typing import List

from pynamodb.connection import Connection
from pynamodb.transactions import TransactWrite, TransactGet

from db.models.user import Fluency, Profile, User, UserRole
from db.models.tables import CoreTableBase, FluencyTable, ImageTable, InvertedIndex, ListTable, MetaCounterTable, ProfileTable, UserAndProfile, UserTable


def create_user_account(user: User) -> UserTable:
    """Create a new user account. Note that this does not add the user to any IAM."""

    user_item = UserTable.from_model(user)
    profile_item = ProfileTable(f"USER#{user.id}", f"USER#{user.id}#PROF", user=user.id, updated=datetime.now())
    user_count = MetaCounterTable("USER##COUNT", "USER##COUNT")

    connection = Connection()
    with TransactWrite(connection=connection) as transaction:
        transaction.save(user_item, condition=(UserTable.pk.does_not_exist() & UserTable.sk.does_not_exist()))
        transaction.save(profile_item)
        transaction.update(user_count, actions=[MetaCounterTable.item_count.add(1)])

    return user_item


def remove_user_account(user: User) -> None:
    """Remove a user account. This is a hard delete that completely removes all user data form the database."""

    user_item = UserTable.from_model(user)
    user_count = MetaCounterTable("USER##COUNT", "USER##COUNT")

    connection = Connection()
    with TransactWrite(connnection=connection) as transaction:
        transaction.delete(user_item, condition=(UserTable.pk.exists() & UserTable.sk.exists()))
        transaction.update(user_count, actions=[MetaCounterTable.item_count.add(-1)])

    with CoreTableBase.batch_write() as batch:
        user_associated_items = CoreTableBase.query(f"USER#{user.id}")
        for item in user_associated_items:
            batch.delete(item)


def update_user_profile(profile: Profile) -> ProfileTable:
    """Update a user's profile. Note that this doesn't require the user model object itself."""

    # Although this is considered an "update" function, it's really just saving over the existing data.
    profile_item = ProfileTable.from_model(profile)
    profile_item.save()

    return profile_item


def get_user_with_profile(user_id: str) -> UserAndProfile:
    """Get a user and that user's profile together."""

    connection = Connection()
    with TransactGet(connnection=connection) as transaction:
        user_future = transaction.get(UserTable, f"USER#{user_id}", f"USER#{user_id}")
        profile_future = transaction.get(UserTable, f"USER#{user_id}", f"USER#{user_id}#PROF")

    return UserAndProfile(user=user_future.get(), profile=profile_future.get())


def get_newest_users(count: int) -> List[UserTable]:
    """Get the newest N users."""

    user_items = UserTable.timestamp_index.query("USER", scan_index_forward=False, limit=count)
    return list(user_items)


def get_lists_of_user(user: User) -> List[ListTable]:
    """Get all of the user's lists."""

    item_list_objects = ListTable.query(f"USER#{user.id}", ListTable.sk.startswith("LIST#"))
    return list(item_list_objects)


def get_images_of_user(user: User) -> List[ImageTable]:
    """Get all of the user's uploaded images."""

    image_items = ImageTable.inverted_index.query(f"USER#{user.id}", InvertedIndex.sk.startswith("IMG#"))
    return list(image_items)


def change_user_privileges(user: User, new_role: UserRole) -> UserTable:
    """Change a user's access privileges."""

    user_item = UserTable.from_model(user)
    user_item.role = new_role.value
    user_item.update(actions=[UserTable.role.set(new_role.value)])


def change_user_fluency(fluency: Fluency) -> FluencyTable:
    """Add or change a user's fluency in a language."""

    fluency_item = FluencyTable.from_model(fluency)
    fluency_item.save()
    return fluency_item

