# DynamoDB access functions for notifications

from typing import List
from db.models.notification import Notification
from db.models.tables import NotificationTable


def create_notification(notification: Notification, user_id: str) -> NotificationTable:
    """Create a new notification for a user."""

    notification_item = NotificationTable.from_model(notification)
    notification_item.pk = f"USER#{user_id}"
    notification_item.save()

    return notification_item


def delete_notification(notification: Notification) -> None:
    """Delete a notification. This is a hard delete that completely removes the notification from the database."""

    notification_item = NotificationTable.from_model(notification)
    notification_item.delete()


def mark_notification_as_read(notification: Notification) -> NotificationTable:
    """Mark a notification as read."""

    notification_item = NotificationTable.from_model(notification)
    notification_item.update(actions=[NotificationTable.read.set(True)])

    return notification_item


def get_notifications_for_user(user_id: str) -> List[NotificationTable]:
    """Get all notifications for a user."""

    notification_items = NotificationTable.query("USER#{user_id}", NotificationTable.sk.startswith("MSG#"))
    return list(notification_items)
