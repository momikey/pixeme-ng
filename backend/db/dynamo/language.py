# DynamoDB access functions for languages

from typing import List
from db.models.language import Language
from db.models.tables import InvertedIndex, LanguageTable, UserTable


def get_list_of_languages() -> List[LanguageTable]:
    """Get the list of languages."""

    language_items = LanguageTable.query("##LANG", LanguageTable.sk.startswith("LANG#"))
    return language_items


def add_language(language: Language) -> LanguageTable:
    """Add a new language."""

    language_item = LanguageTable.from_model(language)
    language_item.save()

    return language_item


def get_users_fluent_in_language(language: Language) -> List[UserTable]:
    """Get all the users who have declared themselves fluent in a language."""

    language_user_items = UserTable.inverted_index.query(f"LANG#{language.code}", InvertedIndex.sk.startswith("USER#"))
    return list(language_user_items)

